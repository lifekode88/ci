<?php

class CES_Workticketnumber_WorkticketController extends Mage_Core_Controller_Front_Action {

    public function updateTicketNumberAction ()
    {
        $data = $this->getRequest()->getParams();
        $orderId    = $data['ticket_order_id'];
        $workTicket = $data['work_ticket'];
        // if your attribute is column named is_active then you can getIsActive() or setIsActive(1)
        $orderModel = Mage::getModel('sales/order')->load($orderId)->setWorkTicket($workTicket);
        // it means you can set and get column named by ['your_attribute']
        try {
            $orderModel->setId($orderId)->save();
            $this->_redirectReferer();
        } catch (Exception $e){
            echo $e->getMessage();
        }
    }
}