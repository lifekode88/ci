<?php
/**
 *
 */
$installer = $this;

$installer->startSetup();

$installer->run("
-- DROP TABLE IF EXISTS {$this->getTable('ask')};
CREATE TABLE {$this->getTable('ask')} (
  `id` int(11) unsigned NOT NULL auto_increment,
  `first_name` varchar(255) NOT NULL default '',
  `last_name` varchar(255) NOT NULL default '',
  `email` varchar(255) NOT NULL default '',
  `question` text NOT NULL default '',
  `image` text  NULL,
  `request_specifics` text  NULL,
  `store_id` int(11) unsigned NOT NULL default 0,
  `created_time` datetime NULL,
  `update_time` datetime NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
    ");
$installer->run("
-- DROP TABLE IF EXISTS {$this->getTable('question')};
CREATE TABLE {$this->getTable('question')} (
  `id` int(11) unsigned NOT NULL auto_increment,
  `content` text NOT NULL default '',
  `created_time` datetime NULL,
  `update_time` datetime NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
    ");

$installer->run("
-- DROP TABLE IF EXISTS {$this->getTable('store')};
CREATE TABLE {$this->getTable('store')} (
  `id` int(11) unsigned NOT NULL auto_increment,
  `store_id` int(11) unsigned NOT NULL default 0,
  `email` text NOT NULL default '',
  `created_time` datetime NULL,
  `update_time` datetime NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
    ");

// Create ask pictures plus folder
$ioFile = new Varien_Io_File();
$featureImagePath = Mage::helper('ask')->getImagePath();
$ioFile->checkAndCreateFolder($featureImagePath);

$installer->endSetup();
