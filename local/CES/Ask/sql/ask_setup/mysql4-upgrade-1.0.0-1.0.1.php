<?php
$installer = $this;

$installer->startSetup();

$installer->run("
ALTER TABLE {$this->getTable('store')}
ADD UNIQUE (store_id);
    ");
$installer->endSetup();
