<?php
$installer = $this;

$installer->startSetup();

$installer->run("
ALTER TABLE {$this->getTable('ask')}
ADD COLUMN `phone` varchar(255) NULL AFTER `last_name`;
    ");
$installer->endSetup();
