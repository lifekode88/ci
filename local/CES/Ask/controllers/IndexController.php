<?php

/**
 * Class CES_Ask_IndexController
 *
 * @author lifekode <lan.nguyen@codeenginestudio.com>
 */
class CES_Ask_IndexController extends Mage_Core_Controller_Front_Action
{
    const OTHER_QUESTION_VALUE = 0;

    public function indexAction()
    {
        $this->loadLayout();
        $this->renderLayout();
    }

    /**
     *
     */
    public function postAction()
    {
        if (!$this->_validateFormKey()) {
            $this->_redirect('*/*');
            return;
        }

        $post = $this->getRequest()->getPost();
        if ($post) {
            try {
                $translate = Mage::getSingleton('core/translate');
                /* @var $translate Mage_Core_Model_Translate */
                $translate->setTranslateInline(false);

                $postObject = new Varien_Object();
                $postObject->setData($post);


                $error = false;
                if (!Zend_Validate::is(trim($post['first_name']), 'NotEmpty')) {
                    $error = true;
                }
                if (!Zend_Validate::is(trim($post['last_name']), 'NotEmpty')) {
                    $error = true;
                }
                if (!Zend_Validate::is(trim($post['question']), 'NotEmpty')) {
                    $error = true;
                }

                if ($post['question'] == self::OTHER_QUESTION_VALUE) {
                    if (!Zend_Validate::is(trim($post['other']), 'NotEmpty')) {
                        $error = true;
                    }
                }

                if (!Zend_Validate::is(trim($post['email']), 'EmailAddress')) {
                    $error = true;
                }

                if (Zend_Validate::is(trim($post['hideit']), 'NotEmpty')) {
                    $error = true;
                }

                if ($error) {
                    throw new Exception();
                }

                $imagePath = $this->uploadImage($_FILES, 'image', Mage::helper('ask')->getImagePath());
                $ccEmail = Mage::getModel('ask/store')->getStoreEmail($post['store_id']);
                $mailTemplate = Mage::getModel('core/email_template');

                $sender['email'] = Mage::getStoreConfig(CES_Ask_Helper_Data::XML_PATH_EMAIL_SENDER);
                $sender['name'] = Mage::getStoreConfig(CES_Ask_Helper_Data::XML_PATH_EMAIL_SENDER_NAME);
                if ($post['question'] == 0) {
                    $postObject->setData('question', $post['other']);
                } else {
                    $postObject->setData('question', Mage::getModel('ask/question')->load($post['question'])->getContent());
                }
                if (!empty($post['phone'])) {
                    $postObject->setData('isShowPhone', 1);
                }
                if (!empty($imagePath)) {
                    $postObject->setData('isShowImage', 1);
                    $postObject->setData('path', $imagePath);
                }
                if (!empty(trim($post['request_specifics']))) {
                    $postObject->setData('isShowRequestSpecifics', 1);
                }
                /* @var $mailTemplate Mage_Core_Model_Email_Template */
                if ($ccEmail != '') {
                    $mailTemplate->getMail()->addCc($ccEmail);
                }
                $mailTemplate->setDesignConfig(array('area' => 'frontend'))
                    ->setReplyTo($post['email'])
                    ->sendTransactional(
                        Mage::getStoreConfig(CES_Ask_Helper_Data::XML_PATH_EMAIL_TEMPLATE),
                        $sender,
                        Mage::getStoreConfig(CES_Ask_Helper_Data::XML_PATH_EMAIL_SENDER),
                        Mage::getStoreConfig(CES_Ask_Helper_Data::XML_PATH_EMAIL_SENDER_NAME),
                        array('data' => $postObject)
                    );

                if (!$mailTemplate->getSentSuccess()) {
                    throw new Exception();
                }

                $translate->setTranslateInline(true);
                Mage::getSingleton('core/session')->addSuccess(Mage::helper('contacts')->__('Your inquiry was submitted and will be responded to as soon as possible. Thank you for contacting us.'));
                $this->_redirect('*/*');
                return;
            } catch (Exception $e) {
                Mage::logException($e);
                $translate->setTranslateInline(true);
                Mage::getSingleton('core/session')->addError(Mage::helper('contacts')->__('Unable to submit your request. Please, try again later'));
                $this->_redirect('*/*');
                return;
            }
        } else {
            //$this->_redirect('contact-us');
            $this->_redirect('*/*');
        }
    }

    /**
     * @param array $fileData
     * @param string $fieldName
     * @return string
     * @throws Exception
     */
    private function uploadImage($fileData, $fieldName, $imagePath)
    {
        if (isset($fileData[$fieldName]['name']) && $fileData[$fieldName]['name'] != '') {
            $uploadPath = Mage::getBaseDir('media') . DS . $imagePath;
            $uploader = new Varien_File_Uploader($fieldName);
            $uploader->setAllowedExtensions(CES_Ask_Helper_Data::FILE_EXTENSIONS);
            $uploader->setAllowRenameFiles(false);
            $uploader->save($uploadPath, $fileData[$fieldName]['name']);
            return $imagePath . $fileData[$fieldName]['name'];
        }
        return '';
    }
}
