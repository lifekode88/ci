<?php

/**
 * Class CES_Ask_Adminhtml_StoreemailController
 *
 * @author lifekode <lan.nguyen@codeenginestudio.com>
 */
class CES_Ask_Adminhtml_StoreemailController extends Mage_Adminhtml_Controller_Action
{

    /**
     * @return $this
     */
    protected function _initAction()
    {
        $this->loadLayout()
            ->_setActiveMenu('ask/store')
            ->_addBreadcrumb(Mage::helper('adminhtml')->__('Store Email Manager'), Mage::helper('adminhtml')->__('Store Email Manager'));
        return $this;
    }

    /**
     *
     */
    public function indexAction()
    {
        $this->_initAction();
        $this->_addContent($this->getLayout()->createBlock('ask/adminhtml_store'));
        $this->renderLayout();
    }

    /**
     *
     */
    public function editAction()
    {
        $storeId = $this->getRequest()->getParam('id');
        $storeModel = Mage::getModel('ask/store')->load($storeId);

        if ($storeModel->getId() || $storeId == 0) {
            Mage::register('ask_store_data', $storeModel);
            $this->loadLayout();
            $this->_setActiveMenu('ask/store');

            $this->_addBreadcrumb(Mage::helper('adminhtml')->__('Item Manager'), Mage::helper('adminhtml')->__('Store Email Manager'));

            $this->getLayout()->getBlock('head')->setCanLoadExtJs(true);

            $this->_addContent($this->getLayout()->createBlock('ask/adminhtml_store_edit'))
                ->_addLeft($this->getLayout()->createBlock('ask/adminhtml_store_edit_tabs'));

            $this->renderLayout();
        } else {
            Mage::getSingleton('adminhtml/session')->addError(Mage::helper('ask')->__('Store Email does not exist'));
            $this->_redirect('*/*/');
        }
    }

    /**
     *
     */
    public function newAction()
    {
        $this->_forward('edit');
    }

    /**
     *
     */
    public function saveAction()
    {
        if ($this->getRequest()->getPost()) {
            try {
                $postData = $this->getRequest()->getPost();
                $storeModel = Mage::getModel('ask/store');

                $storeModel->setId($this->getRequest()->getParam('id'))
                    ->setEmail($postData['email'])
                    ->setStoreId($postData['store_id'])
                    ->save();

                Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('adminhtml')->__('Store Email was successfully saved'));
                Mage::getSingleton('adminhtml/session')->setAskStoreData(false);

                $this->_redirect('*/*/');
                return;
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                Mage::getSingleton('adminhtml/session')->setAskStoreData($this->getRequest()->getPost());
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                return;
            }
        }
        $this->_redirect('*/*/');
    }

    /**
     *
     */
    public function deleteAction()
    {
        if ($this->getRequest()->getParam('id') > 0) {
            try {
                $storeModel = Mage::getModel('ask/store');

                $storeModel->setId($this->getRequest()->getParam('id'))
                    ->delete();
                Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('adminhtml')->__('Store Email was successfully deleted'));
                $this->_redirect('*/*/');
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
            }
        }
        $this->_redirect('*/*/');
    }

    /**
     * Product grid for AJAX request.
     * Sort and filter result for example.
     */
    public function gridAction()
    {
        $this->loadLayout();
        $this->getResponse()->setBody(
            $this->getLayout()->createBlock('ask/adminhtml_store_grid')->toHtml()
        );
    }
}
