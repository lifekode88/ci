<?php

/**
 * Class CES_Ask_Adminhtml_QuestionController
 *
 * @author lifekode <lan.nguyen@codeenginestudio.com>
 */
class CES_Ask_Adminhtml_QuestionController extends Mage_Adminhtml_Controller_Action
{

    /**
     * @return $this
     */
    protected function _initAction()
    {
        $this->loadLayout()
            ->_setActiveMenu('ask/question')
            ->_addBreadcrumb(Mage::helper('adminhtml')->__('Question Manager'), Mage::helper('adminhtml')->__('Question Manager'));
        return $this;
    }

    /**
     *
     */
    public function indexAction()
    {
        $this->_initAction();
        $this->_addContent($this->getLayout()->createBlock('ask/adminhtml_question'));
        $this->renderLayout();
    }

    /**
     *
     */
    public function editAction()
    {
        $storeId = $this->getRequest()->getParam('id');
        $storeModel = Mage::getModel('ask/question')->load($storeId);

        if ($storeModel->getId() || $storeId == 0) {
            Mage::register('ask_question_data', $storeModel);
            $this->loadLayout();
            $this->_setActiveMenu('ask/question');

            $this->_addBreadcrumb(Mage::helper('adminhtml')->__('Question Manager'), Mage::helper('adminhtml')->__('Question Manager'));

            $this->getLayout()->getBlock('head')->setCanLoadExtJs(true);

            $this->_addContent($this->getLayout()->createBlock('ask/adminhtml_question_edit'))
                ->_addLeft($this->getLayout()->createBlock('ask/adminhtml_question_edit_tabs'));

            $this->renderLayout();
        } else {
            Mage::getSingleton('adminhtml/session')->addError(Mage::helper('ask')->__('Question does not exist'));
            $this->_redirect('*/*/');
        }
    }

    /**
     *
     */
    public function newAction()
    {
        $this->_forward('edit');
    }

    /**
     *
     */
    public function saveAction()
    {
        if ($this->getRequest()->getPost()) {
            try {
                $postData = $this->getRequest()->getPost();
                $questionModel = Mage::getModel('ask/question');

                $questionModel->setId($this->getRequest()->getParam('id'))
                    ->setContent($postData['content'])
                    ->save();

                Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('adminhtml')->__('Question was successfully saved'));
                Mage::getSingleton('adminhtml/session')->setAskQuestionData(false);

                $this->_redirect('*/*/');
                return;
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                Mage::getSingleton('adminhtml/session')->setAskQuestionData($this->getRequest()->getPost());
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                return;
            }
        }
        $this->_redirect('*/*/');
    }

    /**
     *
     */
    public function deleteAction()
    {
        if ($this->getRequest()->getParam('id') > 0) {
            try {
                $questionModel = Mage::getModel('ask/question');

                $questionModel->setId($this->getRequest()->getParam('id'))
                    ->delete();
                Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('adminhtml')->__('Question was successfully deleted'));
                $this->_redirect('*/*/');
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
            }
        }
        $this->_redirect('*/*/');
    }

    /**
     * Product grid for AJAX request.
     * Sort and filter result for example.
     */
    public function gridAction()
    {
        $this->loadLayout();
        $this->getResponse()->setBody(
            $this->getLayout()->createBlock('ask/adminhtml_question_grid')->toHtml()
        );
    }
}
