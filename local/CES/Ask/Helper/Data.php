<?php

/**
 * Class CES_Ask_Helper_Data
 * @author lifekode <lan.nguyen@codeenginestudio.com>
 */
class CES_Ask_Helper_Data extends Mage_Core_Helper_Abstract
{
    const XML_PATH_EMAIL_SENDER = 'ask/ask/email';
    const XML_PATH_EMAIL_SENDER_NAME = 'ask/ask/name';
    const XML_PATH_EMAIL_TEMPLATE = 'ask/ask/template';

    const FILE_EXTENSIONS = ['jpg', 'jpeg', 'gif', 'png'];

    /**
     * @return string
     */
    public function getImagePath()
    {
        return Mage::getBaseDir('media') . DS . 'ask' . DS;
    }

}
