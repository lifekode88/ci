<?php

/**
 * Class CES_Ask_Block_Adminhtml_Store
 *
 * @author lifekode <lan.nguyen@codeenginestudio.com>
 */
class CES_Ask_Block_Adminhtml_Store extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    /**
     * CES_Ask_Block_Adminhtml_Store constructor.
     */
    public function __construct()
    {
        $this->_controller = 'adminhtml_store';
        $this->_blockGroup = 'ask';
        $this->_headerText = Mage::helper('ask')->__('Store Email Manager');
        $this->_addButtonLabel = Mage::helper('ask')->__('Add Store Email');
        parent::__construct();
    }
}
