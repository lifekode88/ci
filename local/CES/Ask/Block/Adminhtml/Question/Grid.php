<?php

/**
 * Class CES_Ask_Block_Adminhtml_Question_Grid
 *
 * @author lifekode <lan.nguyen@codeenginestudio.com>
 */
class CES_Ask_Block_Adminhtml_Question_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    /**
     * CES_Ask_Block_Adminhtml_Question_Grid constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->setId('questionGrid');
        $this->setDefaultSort('id');
        $this->setDefaultDir('ASC');
        $this->setSaveParametersInSession(true);
        $this->setUseAjax(true);
    }

    /**
     * @return Mage_Adminhtml_Block_Widget_Grid
     */
    protected function _prepareCollection()
    {
        $collection = Mage::getModel('ask/question')->getCollection();
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    /**
     * @return $this
     */
    protected function _prepareColumns()
    {
        $this->addColumn('id', array(
            'header' => Mage::helper('ask')->__('ID'),
            'align' => 'center',
            'width' => '50px',
            'index' => 'id',
        ));
        $this->addColumn('content', array(
            'header' => Mage::helper('ask')->__('Question'),
            'align' => 'left',
            'index' => 'content',
        ));
        return parent::_prepareColumns();
    }

    /**
     * @param $row
     * @return string
     */
    public function getRowUrl($row)
    {
        return $this->getUrl('*/*/edit', array('id' => $row->getId()));
    }

    /**
     * @return string
     */
    public function getGridUrl()
    {
        return $this->getUrl('*/*/grid', array('_current' => true));
    }
}
