<?php

/**
 * Class CES_Ask_Block_Adminhtml_Question_Edit
 *
 * @author lifekode <lan.nguyen@codeenginestudio.com>
 */
class CES_Ask_Block_Adminhtml_Question_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{

    /**
     * CES_Ask_Block_Adminhtml_Question_Edit constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->_objectId = 'id';
        $this->_blockGroup = 'ask';
        $this->_controller = 'adminhtml_question';
        $this->_updateButton('save', 'label', Mage::helper('ask')->__('Save Question'));
        $this->_updateButton('delete', 'label', Mage::helper('ask')->__('Delete Question'));
    }

    /**
     * @return string
     */
    public function getHeaderText()
    {
        return Mage::helper('ask')->__('Question');
    }
}
