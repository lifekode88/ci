<?php

/**
 * Class CES_Ask_Block_Adminhtml_Question_Edit_Tab_Form
 *
 * @author lifekode <lan.nguyen@codeenginestudio.com>
 */
class CES_Ask_Block_Adminhtml_Question_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
    /**
     * @return Mage_Adminhtml_Block_Widget_Form
     */
    protected function _prepareForm()
    {
        $form = new Varien_Data_Form();
        $this->setForm($form);
        $fieldset = $form->addFieldset('ask_store_form', array('legend' => Mage::helper('ask')->__('Question information')));

        $fieldset->addField('content', 'textarea', array(
            'label' => Mage::helper('ask')->__('Question'),
            'class' => 'required-entry',
            'required' => true,
            'name' => 'content',
        ));

        if (Mage::getSingleton('adminhtml/session')->getAskQuestionData()) {
            $form->setValues(Mage::getSingleton('adminhtml/session')->getAskQuestionData());
            Mage::getSingleton('adminhtml/session')->getAskQuestionData(null);
        } elseif (Mage::registry('ask_question_data')) {
            $form->setValues(Mage::registry('ask_question_data')->getData());
        }
        return parent::_prepareForm();
    }
}
