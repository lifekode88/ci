<?php

/**
 * Class CES_Ask_Block_Adminhtml_Question_Edit_Tabs
 *
 * @author lifekode <lan.nguyen@codeenginestudio.com>
 */
class CES_Ask_Block_Adminhtml_Question_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{

    /**
     * CES_Ask_Block_Adminhtml_Store_Edit_Tabs constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->setId('question_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle(Mage::helper('ask')->__('Question'));
    }

    /**
     * @return Mage_Core_Block_Abstract
     */
    protected function _beforeToHtml()
    {
        $this->addTab('form_section', array(
            'label' => Mage::helper('ask')->__('Question'),
            'title' => Mage::helper('ask')->__('Question'),
            'content' => $this->getLayout()->createBlock('ask/adminhtml_question_edit_tab_form')->toHtml(),
        ));

        return parent::_beforeToHtml();
    }
}
