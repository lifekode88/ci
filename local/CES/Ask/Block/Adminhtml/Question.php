<?php

/**
 * Class CES_Ask_Block_Adminhtml_Question
 *
 * @author lifekode <lan.nguyen@codeenginestudio.com>
 */
class CES_Ask_Block_Adminhtml_Question extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    /**
     * CES_Ask_Block_Adminhtml_Store constructor.
     */
    public function __construct()
    {
        $this->_controller = 'adminhtml_question';
        $this->_blockGroup = 'ask';
        $this->_headerText = Mage::helper('ask')->__('Question Manager');
        $this->_addButtonLabel = Mage::helper('ask')->__('Question Email');
        parent::__construct();
    }
}
