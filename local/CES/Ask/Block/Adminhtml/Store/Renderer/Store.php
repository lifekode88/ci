<?php

/**
 * Class CES_Ask_Block_Adminhtml_Store_Renderer_Store
 *
 * @author lifekode <lan.nguyen@codeenginestudio.com>
 */
class CES_Ask_Block_Adminhtml_Store_Renderer_Store extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
    /**
     * @param Varien_Object $row
     * @return string
     */
    public function render(Varien_Object $row)
    {
        $storeId = $row->getData($this->getColumn()->getIndex());
        $storeModel = Mage::getModel('pointofsale/pointofsale')->load($storeId);
        return $storeModel->getName();
    }
}
