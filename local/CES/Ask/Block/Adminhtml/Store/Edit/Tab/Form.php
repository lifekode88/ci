<?php

/**
 * Class CES_Ask_Block_Adminhtml_Store_Edit_Tab_Form
 *
 * @author lifekode <lan.nguyen@codeenginestudio.com>
 */
class CES_Ask_Block_Adminhtml_Store_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
    /**
     * @return Mage_Adminhtml_Block_Widget_Form
     */
    protected function _prepareForm()
    {
        $form = new Varien_Data_Form();
        $this->setForm($form);
        $fieldset = $form->addFieldset('ask_store_form', array('legend' => Mage::helper('ask')->__('Store information')));

        $fieldset->addField('store_id', 'select', array(
            'label' => Mage::helper('ask')->__('Store'),
            'class' => 'required-entry',
            'values' => Mage::getModel('ask/store')->toOptionArray(),
            'name' => 'store_id',
        ));

        $fieldset->addField('email', 'text', array(
            'label' => Mage::helper('ask')->__('Email'),
            'class' => 'required-entry validate-email',
            'required' => true,
            'name' => 'email',
        ));

        if (Mage::getSingleton('adminhtml/session')->getAskStoreData()) {
            $form->setValues(Mage::getSingleton('adminhtml/session')->getAskStoreData());
            Mage::getSingleton('adminhtml/session')->setAskStoreData(null);
        } elseif (Mage::registry('ask_store_data')) {
            $form->setValues(Mage::registry('ask_store_data')->getData());
        }
        return parent::_prepareForm();
    }
}
