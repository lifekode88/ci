<?php

/**
 * Class CES_Ask_Block_Adminhtml_Store_Edit_Tabs
 *
 * @author lifekode <lan.nguyen@codeenginestudio.com>
 */
class CES_Ask_Block_Adminhtml_Store_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{

    /**
     * CES_Ask_Block_Adminhtml_Store_Edit_Tabs constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->setId('store_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle(Mage::helper('ask')->__('Store Email'));
    }

    /**
     * @return Mage_Core_Block_Abstract
     */
    protected function _beforeToHtml()
    {
        $this->addTab('form_section', array(
            'label' => Mage::helper('ask')->__('Store Email'),
            'title' => Mage::helper('ask')->__('Store Email'),
            'content' => $this->getLayout()->createBlock('ask/adminhtml_store_edit_tab_form')->toHtml(),
        ));

        return parent::_beforeToHtml();
    }
}
