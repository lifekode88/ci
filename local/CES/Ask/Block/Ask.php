<?php

/**
 * Class CES_Ask_Block_Ask
 *
 * @author lifekode <lan.nguyen@codeenginestudio.com>
 */
class CES_Ask_Block_Ask extends Mage_Core_Block_Template
{
    /**
     * @return array
     */
    public function getQuestions()
    {
        $options[] = ['value' => '', 'label' => Mage::helper('ask')->__('--Please Select--')];
        $questions = Mage::getModel('ask/question')->getCollection();
        foreach ($questions as $question) {
            $options[] = ['value' => $question->getId(), 'label' => $question->getContent()];
        }
        $options[] = ['value' => '0', 'label' => Mage::helper('ask')->__('Other')];
        return $options;
    }

    /**
     * @return array
     */
    public function getPickStores()
    {
        $stores = [];
        $collection = Mage::getModel('pointofsale/pointofsale')->getCollection();
        foreach ($collection as $store) {
            if (!empty(trim($store->getName()))) {
                $address = $store->getName() . ' ';
                $address .= " [ ";
                $o = 0;
                if ($store->getAddress_line_1()) {
                    $address .= $store->getAddress_line_1();
                    $o++;
                }
                if ($store->getAddress_line_2()) {
                    if ($o)
                        $address .= ", ";
                    $address .= $store->getAddress_line_2();
                    $o++;
                }
                if ($store->getCity()) {
                    if ($o)
                        $address .= ", ";
                    $address .= $store->getCity();
                    $o++;
                }
                if ($store->getState()) {
                    if ($o)
                        $address .= ", ";
                    $address .= $store->getState();
                    $o++;
                }
                $address .= " ]";
                $stores[$store->getPlaceId()] = $address;
            }
        }
        return $stores;
    }
}
