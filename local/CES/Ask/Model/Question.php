<?php

/**
 * Class CES_Ask_Model_Question
 *
 * @author lifekode <lan.nguyen@codeenginestudio.com>
 */
class CES_Ask_Model_Question extends Mage_Core_Model_Abstract
{
    /**
     *
     */
    public function _construct()
    {
        parent::_construct();
        $this->_init('ask/question');
    }
}
