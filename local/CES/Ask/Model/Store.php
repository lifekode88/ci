<?php

/**
 * Class CES_Ask_Model_Store
 *
 * @author lifekode <lan.nguyen@codeenginestudio.com>
 */
class CES_Ask_Model_Store extends Mage_Core_Model_Abstract
{
    /**
     *
     */
    public function _construct()
    {
        parent::_construct();
        $this->_init('ask/store');
    }

    /**
     * @return array
     */
    public function toOptionArray()
    {
        $collection = Mage::getModel('pointofsale/pointofsale')->getCollection();
        $option[] = ['value' => 1, 'label' => Mage::helper('ask')->__('--Please Select--')];
        foreach ($collection as $store) {
            if (!empty(trim($store->getName()))) {
                $option[] = ['value' => $store->getPlaceId(), 'label' => $store->getName()];
            }
        }
        return $option;
    }

    /**
     * @param $id
     * @return string
     */
    public function getStoreEmail($id)
    {
        $id = (int)$id;
        if ($id != 0) {
            $model = $this->loadByAttribute('store_id', $id);
            if ($model->getId()) {
                return $model->getEmail();
            }
        }
        return '';
    }

    /**
     * Load entity by attribute
     *
     * @param Mage_Eav_Model_Entity_Attribute_Interface|integer|string|array $attribute
     * @param null|string|array $value
     * @param string $additionalAttributes
     * @return bool|Mage_Catalog_Model_Abstract
     */
    public function loadByAttribute($attribute, $value, $additionalAttributes = '*')
    {
        $collection = $this->getResourceCollection()
            ->addFieldToFilter($attribute, $value)
            ->setPageSize(1)
            ->setCurPage(1);

        foreach ($collection as $object) {
            return $object;
        }
        return false;
    }
}
