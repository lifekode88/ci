<?php

/**
 * Class CES_Ask_Model_Mysql4_Question
 *
 * @author lifekode <lan.nguyen@codeenginestudio.com>
 */
class CES_Ask_Model_Mysql4_Question extends Mage_Core_Model_Mysql4_Abstract
{
    /**
     *
     */
    public function _construct()
    {
        $this->_init('ask/question', 'id');
    }
}
