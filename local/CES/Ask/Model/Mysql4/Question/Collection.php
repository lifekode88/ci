<?php

/**
 * Class CES_Ask_Model_Mysql4_Question_Collection
 *
 * @author lifekode <lan.nguyen@codeenginestudio.com>
 */
class CES_Ask_Model_Mysql4_Question_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract
{
    /**
     *
     */
    public function _construct()
    {
        $this->_init('ask/question');
    }
}
