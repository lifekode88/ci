<?php
require_once 'Mage/Catalog/controllers/CategoryController.php';

class CES_Catalog_CategoryController extends Mage_Catalog_CategoryController {

    public function ajaxProductListAction() {
        $id            = $this->getRequest()->getParam('categoryId');
        $numberProduct = (int) $this->getRequest()->getParam('numberProduct');
        $frameStyle    = $this->getRequest()->getParam('frameStyle');
        $frameColor    = $this->getRequest()->getParam('frameColor');
        $minPrice      = (float) $this->getRequest()->getParam('minPrice');
        $maxPrice      = (float) $this->getRequest()->getParam('maxPrice');
        $baseLimit     = (int) $this->getRequest()->getParam('baseLimit');
        $category = Mage::getModel('catalog/category') ->setStoreId(Mage::app()->getStore()->getId()) ->load($id);
        Mage::unregister('current_category');
        Mage::register('current_category', $category);

        $style = null;
        $color = null;
        
        if($frameStyle != 'all') {
            $style = $frameStyle;
        }
        
        if($frameColor != 'all') {
            $color = $frameColor;
        }

        echo $this->getLayout()->createBlock('catalog/product_list')->setTemplate('catalog/product/list_ajax.phtml')
            ->setCategoryId($id)
            ->setLimitProduct($numberProduct)
            ->setFrameStyle($style)
            ->setFrameColor($color)
            ->setMinPrice($minPrice)
            ->setMaxPrice($maxPrice)
            ->setBaseLimit($baseLimit)
            ->toHtml();
        exit;
    }
}