<?php

/* @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;
$setup = new Mage_Sales_Model_Resource_Setup('core_setup');
$installer->startSetup();

$installer->getConnection()->addColumn($installer->getTable('catalog/product_option_type_value'), 'code', 'varchar(255) NULL DEFAULT NULL');
$installer->endSetup();