<?php

/**
 * Class CES_Image_Helper_Data
 */
require_once Mage::getBaseDir() . DS . 'vendor' . DS . '/autoload.php';
// import the Intervention Image Manager Class
use \Intervention\Image\ImageManager;

class CES_Image_Helper_Data extends Mage_Core_Helper_Abstract
{

    /**
     * @param $width
     * @param $pathOrigin
     * @param $pathSmall
     */
    public static function resizeImage($width, $height, $pathOrigin, $pathSmall)
    {
        // create an image manager instance with favored driver
        $manager = new ImageManager(array('driver' => 'gd'));
        $image = $manager->make($pathOrigin)->resize($width, $height);
        $image->orientate();
        $image->save($pathSmall, 100);
    }

    /**
     * @param $width
     * @param $pathOrigin
     * @param $pathSmall
     */
    public static function orientate($pathOrigin)
    {
        $manager = new ImageManager(array('driver' => 'gd'));
        $image = $manager->make($pathOrigin);
        $image->orientate();
        $image->save($pathOrigin, 100);
    }
}
