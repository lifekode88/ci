<?php
require_once 'Mage/Checkout/controllers/CartController.php';
class CES_Checkout_CartController extends Mage_Checkout_CartController {

    /**
     * Ajax update shipping method from post code
     */
    public function ajaxUpdateShippingAction() {
        $country    = (string) $this->getRequest()->getParam('country_id');
        $postcode   = (string) $this->getRequest()->getParam('estimate_postcode');
        $city       = (string) $this->getRequest()->getParam('estimate_city');
        $regionId   = (string) $this->getRequest()->getParam('region_id');
        $region     = (string) $this->getRequest()->getParam('region');
        $estimateMethod = (string) $this->getRequest()->getParam('estimate_method');
        $this->_getQuote()->getShippingAddress()
            ->setCountryId($country)
            ->setCity($city)
            ->setPostcode($postcode)
            ->setRegionId($regionId)
            ->setRegion($region)
            ->setCollectShippingRates(true);
        $this->_getQuote()->save();
        if($estimateMethod == 'tablerate_bestway') {
            $this->ajaxEstimateUpdatePostAction($estimateMethod);
        } else {
            echo $estimateMethod;
        }
    }

    /**
     * Estimate update action
     * @return null
     */
    public function ajaxEstimateUpdatePostAction($estimateMethod)
    {
        $code = $estimateMethod;
        if (!empty($code)) {
            $this->_getQuote()->getShippingAddress()->setShippingMethod($code)/*->collectTotals()*/->save();
            echo $estimateMethod;
        } else {
            echo null;
        }
    }

    /**
     * Get state code if region Id = Integer
     * @return mixed
     */
    public function ajaxGetRegionCodeAction() {
        $regionId    = (string) $this->getRequest()->getParam('region_id');
        $region = Mage::getModel('directory/region')->load($regionId);
        $stateCode = $region->getCode();
        echo $stateCode;
    }

    /**
     * Store summary data on session after next step
     * @return bool
     */
    public function ajaxKeepSummaryDataAction() {
        $method   = (string) $this->getRequest()->getParam('method');
        $summary  = (array) $this->getRequest()->getParam('summary');
        $setMethod = 'setSummary'.$method;
        Mage::getSingleton('core/session')->$setMethod($summary);
        return true;
    }

    /**
     * Clear summary data on place order
     */
    public function ajaxClearSummaryDataAction() {
        $methods = $this->getRequest()->getParam('methods');
        foreach($methods as $method) {
            $setMethod = 'setSummary'.$method;
            Mage::getSingleton('core/session')->$setMethod(null);
        }
    }

    /**
     * Initialize coupon
     */
    public function couponPostAction()
    {
        /**
         * No reason continue with empty shopping cart
         */
        if (!$this->_getCart()->getQuote()->getItemsCount()) {
            $this->_goBack();
            return;
        }

        $couponCode = (string) $this->getRequest()->getParam('coupon_code');
        $page = (string) $this->getRequest()->getParam('current-page');
        if ($this->getRequest()->getParam('remove') == 1) {
            $couponCode = '';
        }
        $oldCouponCode = $this->_getQuote()->getCouponCode();

        if (!strlen($couponCode) && !strlen($oldCouponCode)) {
            $this->_goBack();
            return;
        }

        try {
            $codeLength = strlen($couponCode);
            $isCodeLengthValid = $codeLength && $codeLength <= Mage_Checkout_Helper_Cart::COUPON_CODE_MAX_LENGTH;

            $this->_getQuote()->getShippingAddress()->setCollectShippingRates(true);
            $this->_getQuote()->setCouponCode($isCodeLengthValid ? $couponCode : '')
                ->collectTotals()
                ->save();

            if ($codeLength) {
                if ($isCodeLengthValid && $couponCode == $this->_getQuote()->getCouponCode()) {
                    $this->_getSession()->addSuccess(
                        $this->__('Coupon code "%s" was applied.', Mage::helper('core')->escapeHtml($couponCode))
                    );
                    Mage::getSingleton('core/session')->setSuccessCoupon($this->__('Coupon code "%s" was applied.', Mage::helper('core')->escapeHtml($couponCode)));
                } else {
                    $this->_getSession()->addError(
                        $this->__('Coupon code "%s" is not valid.', Mage::helper('core')->escapeHtml($couponCode))
                    );
                    Mage::getSingleton('core/session')->setSuccessCoupon($this->__('Coupon code "%s" is not valid.', Mage::helper('core')->escapeHtml($couponCode)));
                }
            } else {
                $this->_getSession()->addSuccess($this->__('Coupon code was canceled.'));
                Mage::getSingleton('core/session')->setSuccessCoupon($this->__('Coupon code was canceled.'));
            }

        } catch (Mage_Core_Exception $e) {
            $this->_getSession()->addError($e->getMessage());
            Mage::getSingleton('core/session')->setErrorCoupon($e->getMessage());
        } catch (Exception $e) {
            $this->_getSession()->addError($this->__('Cannot apply the coupon code.'));
            Mage::getSingleton('core/session')->setErrorCoupon($this->__('Cannot apply the coupon code.'));
            Mage::logException($e);
        }

        if($page == 'cart') {
            $this->_goBack();
        } else {
            $this->_redirect('checkout/onepage');
        }
    }

    /**
     * Delete shoping cart item action
     */
    public function deleteAction()
    {
        if ($this->_validateFormKey()) {
            $id = (int)$this->getRequest()->getParam('id');
            if ($id) {
                try {
                    $this->_getCart()->removeItem($id)
                        ->save();
                } catch (Exception $e) {
                    $this->_getSession()->addError($this->__('Cannot remove the item.'));
                    Mage::logException($e);
                }
            }
        } else {
            $this->_getSession()->addError($this->__('Cannot remove the item.'));
        }
        $this->_redirect('checkout/cart');
    }

    /**
     * Render cart total by layout
     * @return mixed
     */
    public function ajaxCartTotalAction() {
        
        $totalsBlock = $this->getLayout()->createBlock('checkout/cart_totals')->setTemplate('checkout/cart/sidebar_subtotal.phtml');
        echo $totalsBlock->toHtml();
        exit;
    }
}