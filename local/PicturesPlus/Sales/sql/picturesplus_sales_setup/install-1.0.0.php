<?php
$installer = $this;
$installer->startSetup();
$setup = new Mage_Sales_Model_Resource_Setup('core_setup');
$installer->getConnection()->addColumn($installer->getTable('sales_flat_order'), 'work_ticket', 'varchar(255) NULL DEFAULT NULL');
$setup->addAttribute('order', 'work_ticket', array('type' => 'static', 'visible' => false));

$installer->endSetup();