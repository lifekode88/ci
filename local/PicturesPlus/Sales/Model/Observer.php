<?php

class PicturePlus_Sales_Model_Observer
{
    public function saveCustomData($event)
    {
        $quote = $event->getSession()->getQuote();
        $quote->setData('work_ticket', $event->getRequestModel()->getPost('work_ticket'));
        return $this;
    }
}