<?php
$this->addAttribute('customer', 'phone_number', array(
    'type'      => 'varchar',
    'label'     => 'Phone Number',
    'input'     => 'text',
    'position'  => 120,
    'required'  => false,//or true
    'is_system' => 0,
));
$phone_attribute = Mage::getSingleton('eav/config')->getAttribute('customer', 'phone_number');
$phone_attribute->setData('used_in_forms', array(
    'adminhtml_customer',
    'checkout_register',
    'customer_account_create',
    'customer_account_edit',
));
$phone_attribute->setData('is_user_defined', 0);
$phone_attribute->save();


$this->addAttribute('customer', 'mailing_address', array(
    'type'      => 'varchar',
    'label'     => 'Mailing Address',
    'input'     => 'text',
    'position'  => 120,
    'required'  => false,//or true
    'is_system' => 0,
));
$mailing_attribute = Mage::getSingleton('eav/config')->getAttribute('customer', 'mailing_address');
$mailing_attribute->setData('used_in_forms', array(
    'adminhtml_customer',
    'checkout_register',
    'customer_account_create',
    'customer_account_edit',
));
$mailing_attribute->setData('is_user_defined', 0);
$mailing_attribute->save();