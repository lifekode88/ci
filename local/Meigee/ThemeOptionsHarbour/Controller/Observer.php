<?php
/**
 * Magento
 *
 * @author    Meigeeteam http://www.meaigeeteam.com <nick@meaigeeteam.com>
 * @copyright Copyright (C) 2010 - 2012 Meigeeteam
 *
 */
class Meigee_ThemeOptionsHarbour_Controller_Observer
{
	//Event: adminhtml_controller_action_predispatch_start
	public function overrideTheme()
	{
		Mage::getDesign()->setArea('adminhtml')
			->setTheme('harbour');
	}



    public function checkoutCartProductAddAfter(Varien_Event_Observer $observer)
    {
       $event = $observer->getEvent();
       $quote_item = $event->getQuoteItem();
       $myWeight = $quote_item->getWeight() + 1;

       $quote_item->getProduct()->setIsSuperMode(true);

       $quote_item->setWeight($myWeight);
    }
}
